#!/usr/bin/env python3
import tkinter as tk
import tkinter.ttk as ttk

class SignUpForm(tk.Toplevel):

    def __init__(self, master):
        super().__init__(master)
        self.withdraw()     # hide until ready to show
        self.title("Sign Up")
        self.signuphereLabel = ttk.Label(self, text="Sign up here!")
        self.signuphereLabel.grid(row=0, column=0, columnspan=5)
        self.fullNameLabel = ttk.Label(self, text="Full Name:")
        self.fullNameLabel.grid(row=1, column=0, sticky=tk.W, padx="0.75m", pady="0.75m")
        self.fullnameEntry = ttk.Entry(self)
        self.fullnameEntry.grid(row=1, column=1, sticky=(tk.W, tk.E), padx="0.75m", pady="0.75m")
        self.emailAddressLabel = ttk.Label(self, text="Email Address:")
        self.emailAddressLabel.grid(row=2, column=0, sticky=tk.W, padx="0.75m", pady="0.75m")
        self.emailEntry = ttk.Entry(self)
        self.emailEntry.grid(row=2, column=1, sticky=(tk.W, tk.E), padx="0.75m", pady="0.75m")
        self.signmeupButton = ttk.Button(self, text="Sign me up!")
        self.signmeupButton.grid(row=3, column=0, padx="0.75m", pady="0.75m")
        self.cancelButton = ttk.Button(self, text="Cancel")
        self.cancelButton.grid(row=3, column=1, padx="0.75m", pady="0.75m")
        self.bind("<Escape>", lambda *args: self.destroy())
        self.deiconify()    # show when widgets are created and laid out
        if self.winfo_viewable():
            self.transient(master)
        self.wait_visibility()
        self.grab_set()
        self.wait_window(self)

if __name__ == "__main__":
    application = tk.Tk()
    window = SignUpForm(application)
    application.protocol("WM_DELETE_WINDOW", application.quit)
    application.mainloop()
